﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SleepCycleCalculator.Tests
{
    [TestFixture]
    public class SleepCalculatorControllerTests
    {
        [Test]
        public void CalculateBedTime_WithNullDateTime_ThrowsArgumentNullExceptio()
        {
        
        }

        [Test]
        public void CalculateBedTime_WithDateInPast_ThrowsInvalidOperationException()
        { 
            
        }

        [Test]
        public void CalculateBedTime_WithValidDate_ReturnsAnArrayOfSleepCycleObjects()
        {

        }

        [Test]
        public void CalculateWakeUpTime_ReturnsAnArrayOfSleepCycleObjects()
        { 
        
        }

        
    }
}
